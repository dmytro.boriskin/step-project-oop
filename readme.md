# step-project-oop

## About step-project-oop

This is a third Step project is made as part of a training course in DAN.IT education training center.
The main task of the project is OOP in JS.
Step-project-oop is an exmaple of website with some desing, but JS OOP oriented.   
This project manages its dependencies using npm. 
This project is build usind package builder Gulp. 

Project deployment - skeleton, html and css, and drug and drop function are done by Dmytro. 
Main program logic, such as visit card and modals window are done by Oleksandr.  

## The latest version

Details of the last version can be found on the gitlab repository https://gitlab.com/dmytro.boriskin/step-project-oop/tree/master/

## Liсence

step-project-oop is open-source software licensed under the ISC License.

## Contacts 

You can find us in Slack chat: 
Oleksandr Shtuka  @ Shtuka Oleksandr
Dmytro Boriskin @ dmytro