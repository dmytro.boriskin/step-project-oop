class Visit {
    constructor(doctor,
                date = "date of your visit",
                patientName = "your name",
                doctorName = "name of your doctor",
                visitPurpose = "your visit purpose",
                message = "your message",
                id)
    {
        this.doctor = doctor;
        this.patientName = patientName;
        this.dateOfVisit = date;
        this.doctorName = doctorName;
        this.visitPurpose = visitPurpose;
        this.message = message;
        this.id = id;
    }

    static offSetLeft = 0;
    static offSetTop = 0;

    static createModalWindow () {
        const modalWindow = document.createElement("form");
        modalWindow.id = "modalWindow";
        modalWindow.classList.add("form");
        modalWindow.classList.add("hide");
        modalWindow.innerHTML = " <select class=\"select\" id=\"doctor\">\n" +
            "                        <option value=\"none\">Choose doctor</option>\n" +
            "                        <option value=\"cardiologist\">Cardiologist</option>\n" +
            "                        <option value=\"dentist\">Dentist</option>\n" +
            "                        <option value=\"therapist\">Therapist</option>\n" +
            "                     </select>\n" +
            "                     <button class=\"closeButton\" id=\"closeButton\">X</button>\n" +
            "                     <div class=\"line\"></div>\n" +
            "                     <button class=\"form-button\">Create</button>"
        document.querySelector(".modalWindowContainer").appendChild(modalWindow);
    }

    static clearInputForm () {
        const inputForm = document.querySelector(".inputFormWr");
        if (inputForm) {
            inputForm.remove();
        }
        const doctor = document.getElementById("doctor");
        doctor.options.selectedIndex = 0;
    }

    static createInputField (id, placeholder) {
        const inputField = document.createElement("input");
        inputField.classList.add("name");
        inputField.type = "text";
        inputField.id = id;
        inputField.placeholder = placeholder;
        return inputField;
    }

    static createTextarea () {
        const textarea = document.createElement("textarea");
        textarea.classList.add("textarea");
        textarea.id = "message";
        textarea.placeholder = "Your message";
        textarea.rows = 10;
        textarea.maxLength = 400;
        return textarea;
    }

    static createInfoStringForCard (label, info) {
        const str  = document.createElement("p");
        str.classList.add("marginBetweenStr");
        str.innerText = `${label} : ${info}`;
        return str;
    }

    static getVisitObgFromLocalStorage (id) {
        let obj = localStorage.getItem(id);
        obj = JSON.parse(obj);
        return obj;
    }

    static showAllInfo (container, obj, link){
        const infoWr = document.createElement("div");
        infoWr.classList.add("info");
        for (let key in obj) {
            if (key !== "doctor" && key !== "doctorName" && key !== "id") {
                infoWr.appendChild(Visit.createInfoStringForCard(key, obj[key]));
            }
        }
        container.insertBefore(infoWr, link);
        container.style.zIndex = 1000;
        link.innerText = "Hide";
        link.classList.replace("more", "less");
    }

    static hideInfo (infoContainer, link) {
        infoContainer.parentElement.style.zIndex = 0;
        infoContainer.remove();
        link.innerText = "Show more";
        link.classList.replace("less", "more");
    }

    static deleteCard (id) {
        const card = document.getElementById(id);
        card.remove();
        localStorage.removeItem(id);
        if (localStorage.length === 0) {
            document.querySelector(".no-items").classList.remove("hide");
        }
    }

    static createCardFromLocalStorageObj (obj) {
        const wr = document.createElement("div");
        wr.innerHTML = `<div class="card" draggable="true" id="${obj.id}" style="position: absolute; left: ${Visit.offSetLeft}px; top: ${Visit.offSetTop}px;">
                        <p class="fullName">${obj.doctorName}</p>
                        <p class="marginBetweenStr">${obj.doctor}</p>
                        <a href="#" class="more">Show more</a>
                        <button class="closeButton-card">X</button>`;
        document.querySelector(".main-section").appendChild(wr);
        document.querySelector(".no-items").classList.add("hide");
        Visit.findCardCoordination(document.getElementById(`${obj.id}`));
    }

    static showCardFromLocalStorage () {
        if (localStorage.length > 0) {
            for (let i = 0; i < localStorage.length; i++) {
                let obj = Visit.getVisitObgFromLocalStorage(localStorage.key(i));
                Visit.createCardFromLocalStorageObj(obj);
            }
            document.querySelector(".no-items").classList.add("hide");
        }
    }

    static findCardCoordination (card) {
        Visit.offSetLeft += card.offsetWidth + 10;
        if (Visit.offSetLeft > 1000) {
            Visit.offSetLeft = 0;
            Visit.offSetTop += card.offsetHeight + 10;
        }
    }

    static dragNdrop (card) {
        card.addEventListener("dragstart", dragStart);
        card.addEventListener("dragend", dragEnd);

        function dragStart() {
            document.body.appendChild(this);
        }
        
        function dragEnd() {
            card.style.top = event.clientY - card.offsetHeight + "px";
            card.style.left = event.clientX - card.offsetWidth +  "px";
        }
    }

    createInputForm () {
        const inputFormWr = document.createElement("div");
        inputFormWr.classList.add("inputFormWr");
        for (let key in this) {
            if (key !== "doctor" && key !== "message" && key !== "id") {
                inputFormWr.appendChild(Visit.createInputField(key, this[key]));
            }
        }
        inputFormWr.appendChild(Visit.createTextarea());
        document.querySelector(".form").insertBefore(inputFormWr, document.querySelector(".line"));
    }

    checkInputField () {
        for (let key in this) {
            if (key !== "id" && !document.getElementById(`${key}`).value && key !== "message"){
                document.getElementById(`${key}`).placeholder = "FILL THIS FIELD!!!!"
                return false;
            }
        }
        return true;
    }

    createVisitObj () {
        for (let key in this) {
            if (key !== "doctor" && key !== "id") {
                this[key] = document.getElementById(`${key}`).value;
            }
        }
        this.id = new Date().getTime();
    }

    putVisitObjInLocalStorage () {
        localStorage.setItem(`${this.id}`, JSON.stringify(this));
    }

    createCardFromVisitObj () {
        const wr = document.createElement("div");
        wr.innerHTML = `<div class="card" draggable="true" id="${this.id}" style="position: absolute; left: ${Visit.offSetLeft}px; top: ${Visit.offSetTop}px;">
                        <p class="fullName">${this.doctorName}</p>
                        <p class="marginBetweenStr">${this.doctor}</p>
                        <a href="#" class="more">Show more</a>
                        <button class="closeButton-card">X</button>`;
        document.querySelector(".main-section").appendChild(wr);
        document.querySelector(".no-items").classList.add("hide");
        Visit.findCardCoordination(document.getElementById(`${this.id}`));
    }

    createCard () {
        if (this.checkInputField()) {
            this.createVisitObj();
            this.putVisitObjInLocalStorage();
            this.createCardFromVisitObj();
            Visit.clearInputForm();
            hideElement(document.querySelector(".form"));
        }
    }

}

class Cardiologist extends Visit {
    constructor(doctor, date, patientName, doctorName, visitPurpose,
                age = "your age",
                commonPressure = "your common pressure",
                bodyMassIndex  = "your body mass index",
                diseasesCardioSystem = "your cardiosystem diseases")
    {
        super("Cardiologist", date, patientName, doctorName, visitPurpose);
        this.patientAge = age;
        this.commonPressure = commonPressure;
        this.bodyMassIndex = bodyMassIndex;
        this.diseasesCardioSystem = diseasesCardioSystem;
    }
}

class Dentist extends Visit {
    constructor(doctor, date, patientName, visitPurpose, lastVisitDate = "your last visit date", doctorName) {
        super("Dentist", date, patientName, doctorName, visitPurpose);
        this.lastVisitDate = lastVisitDate;
    }
}

class Therapist extends Visit {
    constructor(doctor, date, patientName, visitPurpose, age = "your age", doctorName) {
        super("Therapist", date, patientName, doctorName, visitPurpose);
        this.patientAge = age;
    }
}

const hideElement = function(element) {
    element.classList.add("hide");
};

const showElement = function (element) {
    element.classList.remove("hide");
}

Visit.showCardFromLocalStorage();

Visit.createModalWindow();
let doctorName = "";

const doctorSelect = document.querySelector(".select");
doctorSelect.addEventListener("change", function () {
    const doctor = document.getElementById("doctor");
    doctorName = doctor.options[doctor.selectedIndex].value;
    if (document.querySelector(".inputFormWr")) {
        Visit.clearInputForm();
    } else if (doctorName === "cardiologist") {
        new Cardiologist().createInputForm();
    } else if (doctorName === "dentist") {
        new Dentist().createInputForm();
    } else if (doctorName === "therapist") {
        new Therapist().createInputForm();
    } else {
        Visit.clearInputForm();
    }
})

const mainListener = document.querySelector("body");
mainListener.addEventListener("click", function (event) {
    if (event.target.className.includes("create-button")) {
        showElement(document.querySelector(".form"));
    } else if (event.target.className.includes("form-button")) {
        event.preventDefault();
        location.reload();
        if (doctorName === "cardiologist") {
           new Cardiologist().createCard();
        } else if (doctorName === "dentist") {
            new Dentist().createCard();
        } else if (doctorName === "therapist"){
           new Therapist().createCard();
        }
    } else if (event.target.className.includes("more")){
        let card = event.target.parentElement;
        let objFromLocalStorage = Visit.getVisitObgFromLocalStorage(card.id);
        Visit.showAllInfo(card, objFromLocalStorage, event.target);
    } else if (event.target.className.includes("less")) {
        Visit.hideInfo(event.target.previousElementSibling, event.target);
    } else if (event.target.className.includes("closeButton-card")) {
        Visit.deleteCard(event.target.parentElement.id);
    } else if (event.target.className.includes("closeButton")) {
        hideElement(document.querySelector(".form"));
        Visit.clearInputForm();
    } else if (!event.target.className.includes("form")&&
                !event.target.className.includes("select")&&
                !event.target.className.includes("line")&&
                !event.target.className.includes("name")&&
                !event.target.className.includes("textarea")&&
                !event.target.className.includes("inputFormWr"))
    {
        hideElement(document.querySelector(".form"));
        Visit.clearInputForm();
    }
});

mainListener.addEventListener("mouseover", function (event) {
    if (event.target.className.includes("card")) {
        Visit.dragNdrop(event.target);
    }
});






